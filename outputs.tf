output "instance-profile" {
  description = "id of the instance profile data and its tweaks"
  value       = module.instance-profile.instance_profile_id

}
output "launch_template_id" {
  description = "launch template id and all attributes"
  value       = module.launch_template.launch_template
}
output "autoscaling_group_id" {
  description = "autoscaling group id"
  value       = module.asg.aws_autoscaling_group
}
output "dns_name_lb" {
  description = "prints the domain name of load balancer updation"
  value       = module.elb_asg.dns_name
}


